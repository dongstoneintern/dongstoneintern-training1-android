package com.demoapp.alcodesonboard.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriend;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder> {

    private List<MyFriend> myFriendsList;
    private Callbacks mCallbacks;

    private Context context;

    public MyFriendsAdapter(List<MyFriend> myFriendsList, Context context) {
        this.myFriendsList = myFriendsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyFriend myFriend = myFriendsList.get(position);
        holder.friendName.setText(myFriend.getFirstName() +" " + myFriend.getLastName());
        holder.friendEmail.setText((myFriend.getEmail()));
        Glide.with(context).load(myFriend.getImage()).into(holder.picture);

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                Intent intent = new Intent(context, MyFriendDetailActivity.class);
                Long l = new Long(position);
                intent.putExtra(MyFriendDetailActivity.EXTRA_LONG_MY_FRIEND_ID, l);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myFriendsList.size();
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String firstName;
        public String lastName;
        public String friendEmail;
        public String imageURL;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relativelayout_friend_root)
        public RelativeLayout root;

        @BindView(R.id.imageView2)
        public ImageView picture;

        @BindView(R.id.textview_friend_name)
        public TextView friendName;

        @BindView(R.id.textview_friend_email)
        public TextView friendEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                friendName.setText(data.firstName);
                friendEmail.setText((data.friendEmail));

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }

        public void resetViews() {
            friendName.setText("");
            friendEmail.setText((""));
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(MyFriendsAdapter.DataHolder data);

    }
}
