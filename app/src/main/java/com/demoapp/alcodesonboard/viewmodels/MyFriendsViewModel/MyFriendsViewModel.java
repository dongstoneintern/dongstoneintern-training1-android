package com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;

import java.util.List;

public class MyFriendsViewModel extends AndroidViewModel {

    private MyFriendsRepository mMyFriendsRepository;

    public MyFriendsViewModel(@NonNull Application application) {
        super(application);

        mMyFriendsRepository = MyFriendsRepository.getInstance();
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsRepository.getMyFriendsAdapterListLiveData();
    }

    public void loadMyFriendsAdapterList() {
        mMyFriendsRepository.loadMyFriendsAdapterList(getApplication());
    }
    public void addFriend(String first_name, String last_name, String email, String image) {
        mMyFriendsRepository.addFriend(getApplication(), first_name, last_name, email, image);
    }
}
