package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.database.entities.MyFriendDao;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class MyFriendsRepository {

    private static MyFriendsRepository mInstance;

    private MutableLiveData<List<MyFriendsAdapter.DataHolder>> mMyFriendsAdapterListLiveData = new MutableLiveData<>();

    public static MyFriendsRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendsRepository.class) {
                mInstance = new MyFriendsRepository();
            }
        }

        return mInstance;
    }

    public MyFriendsRepository() {
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsAdapterListLiveData;
    }

    public void loadMyFriendsAdapterList(Context context) {
        List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if (records != null) {
            for (MyFriend myFriend : records) {
                MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                dataHolder.id = myFriend.getId();
                dataHolder.firstName = myFriend.getFirstName();
                dataHolder.lastName = myFriend.getLastName();
                dataHolder.friendEmail = myFriend.getEmail();
                dataHolder.imageURL = myFriend.getImage();

                dataHolders.add(dataHolder);
            }
        }

        mMyFriendsAdapterListLiveData.setValue(dataHolders);
    }

    public String getFirstName(Context context, Long id) {
        MyFriendDao myFriendDao = DatabaseHelper.getInstance(context).getMyFriendDao();
        MyFriend myFriend = myFriendDao.load(id);
        if(myFriend != null){
            return myFriend.getFirstName();
        }
        return null;
    }

    public String getLastName(Context context, Long id){
        MyFriendDao myFriendDao = DatabaseHelper.getInstance(context).getMyFriendDao();
        MyFriend myFriend = myFriendDao.load(id);
        if(myFriend != null){
            return myFriend.getLastName();
        }
        return null;
    }
    public String getFriendEmail(Context context, Long id){
        MyFriendDao myFriendDao = DatabaseHelper.getInstance(context).getMyFriendDao();
        MyFriend myFriend = myFriendDao.load(id);
        if(myFriend != null){
            return myFriend.getEmail();
        }
        return null;
    }

    public String getImage(Context context, Long id){
        MyFriendDao myFriendDao = DatabaseHelper.getInstance(context).getMyFriendDao();
        MyFriend myFriend = myFriendDao.load(id);
        if(myFriend != null){
            return myFriend.getImage();
        }
        return null;
    }

    public void addFriend(Context context, String first_name, String last_name, String email, String image) {
        // Create new records
        MyFriend myFriend = new MyFriend();
        myFriend.setFirstName(first_name);
        myFriend.setLastName(last_name);
        myFriend.setEmail(email);
        myFriend.setImage(image);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .insert(myFriend);

        // Done adding record, now re-load list.
        loadMyFriendsAdapterList(context);
    }
}
