package com.demoapp.alcodesonboard.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class MyFriendDetailFragment extends Fragment {

    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIEND_ID = "ARG_LONG_MY_FRIEND_ID";
    private String image;
    private Context context;

    @BindView(R.id.firstName)
    protected TextView mFirstName;

    @BindView(R.id.lastName)
    protected TextView mLastName;

    @BindView(R.id.friendEmail)
    protected TextView mFriendEmail;

    @BindView(R.id.imageView_friend_detail)
    protected ImageView mFriendImage;

    private Unbinder mUnbinder;
    private Long mMyFriendId = 0L;
    public MyFriendDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyFriendDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIEND_ID, id);

        MyFriendDetailFragment fragment = new MyFriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFriendDetail();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyFriendId = args.getLong(ARG_LONG_MY_FRIEND_ID, 0);
        }

        getFriendDetail();
    }
    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note.
    }
//    private void initView() {
//        // TODO BAD practice, should move Database operations to Repository.
//        MyFriendsRepository myFriend = new MyFriendsRepository();
//        String firstName = MyFriendsRepository.getInstance().getFirstName(getActivity(), mMyFriendId);
//        String lastName = MyFriendsRepository.getInstance().getLastName(getActivity(), mMyFriendId);
//        String friendEmail = MyFriendsRepository.getInstance().getFriendEmail(getActivity(), mMyFriendId);
//        String image = MyFriendsRepository.getInstance().getImage(getActivity(), mMyFriendId);
//        if (mMyFriendId > 0) {
//
//            if (myFriend != null) {
//                mFirstName.setText(firstName);
//                mLastName.setText(lastName);
//                mFriendEmail.setText(friendEmail);
////                mFriendImage.setImageURI(image);
//            } else {
//                // Record not found.
//                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
//                getActivity().finish();
//            }
//        }
//    }

    private void getFriendDetail() {

        final ProgressDialog progressDialog = new ProgressDialog(this.getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        // Call login API.
        mMyFriendId += 1;
        String url = BuildConfig.BASE_API_URL + "users/" + mMyFriendId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                // Convert JSON string to Java object.
                Toast.makeText(getContext(), "Response : " + response.toString(), Toast.LENGTH_LONG);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jo = jsonObject.getJSONObject("data");

                        mFirstName.setText(jo.getString("first_name"));
                        mLastName.setText(jo.getString("last_name"));
                        mFriendEmail.setText(jo.getString("email"));
                        Glide.with(getContext()).load(jo.getString("avatar")).into(mFriendImage);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("Response error: %s", error.getMessage());
            }
        });

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }

}
