package com.demoapp.alcodesonboard.fragments;

import android.animation.Animator;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class MyFriendsFragment extends Fragment implements MyFriendsAdapter.Callbacks {
    public static final String TAG = MyFriendsFragment.class.getSimpleName();

    @BindView(R.id.friend_recyclerview)
    protected RecyclerView fRecyclerView;

    private Animator currentAnimator;
    private Unbinder mUnbinder;
    private MyFriendsAdapter mfAdapter;
    private MyFriendsViewModel mfViewModel;
    private int shortAnimationDuration;

    ArrayList<MyFriend> myFriendsList = new ArrayList<>();

    public static final String EXTRA_LONG_MY_FRIEND_ID = "EXTRA_LONG_MY_FRIEND_ID";

    private final int REQUEST_CODE_MY_FRIENDS_DETAIL = 300;

    public MyFriendsFragment() {
        // Required empty public constructor
    }

    public static MyFriendsFragment newInstance() {
        return new MyFriendsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myFriendsList = new ArrayList<>();
        getUserAPI();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getUserAPI() {
        ArrayList<MyFriend> myFriendsList = new ArrayList<>();

        final ProgressDialog progressDialog = new ProgressDialog(this.getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        // Call login API.
        String url = BuildConfig.BASE_API_URL + "users?page=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                // Convert JSON string to Java object.
                Toast.makeText(getContext(), "Response : " + response.toString(), Toast.LENGTH_LONG);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("data");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jo = array.getJSONObject(i);

                        MyFriend myFriend = new MyFriend();
                        myFriend.setFirstName(jo.getString("first_name"));
                        myFriend.setLastName(jo.getString("last_name"));
                        myFriend.setEmail(jo.getString("email"));
                        myFriend.setImage(jo.getString("avatar"));

                        myFriendsList.add(myFriend);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mfAdapter = new MyFriendsAdapter(myFriendsList, getContext());
                fRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
                fRecyclerView.setHasFixedSize(true);
                fRecyclerView.setAdapter(mfAdapter);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("Response error: %s", error.getMessage());
            }
        });

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onListItemClicked(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra(MyFriendDetailActivity.EXTRA_LONG_MY_FRIEND_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIENDS_DETAIL);
    }

}